#include<iostream>
#include<fstream>
#include<math.h>

using namespace std;

int factorial(int numero)
{
  int temp;
  if (numero<=1) return 1;
  temp=numero*factorial(numero-1);
  return temp;
}

float potencia(double base,int exponente)
{
  float valor=1.0;
  for (int i=0;i<exponente;i++)
    {
      valor=valor*base;
    }
  return valor;
}

int main()
{
  ofstream datosbajos, datosaltos;
  long double valorbajo, valoralto;
  double basebaja=0.1, basealta=1.1, estimacionbaja=1.0, estimacionalta=1.0, diferenciaporcentualbaja=0.0, diferenciaporcentualalta=0.0;
  valorbajo=exp(basebaja);
  valoralto=exp(basealta);
  datosbajos.open("datosbajosdoubles.dat");
  cout<<"\nUnderflow\n";
  cout<<"Numero de operacion 0; estimacion 1"<<endl;
  for(int i=1;i<20;i++)
    {
      estimacionbaja=estimacionbaja+(potencia(basebaja,i)/factorial(i));
      diferenciaporcentualbaja=100*(valorbajo-estimacionbaja)/valorbajo;
      cout<<"Numero de operacion "<<i<<"; estimacion "<<estimacionbaja<<"; diferencia porcentual "<<diferenciaporcentualbaja<<endl;
      datosbajos<<diferenciaporcentualbaja<<endl;
    }
  datosbajos.close();
  cout<<"\nOverflow\n";
  cout<<"Numero de operacion 0; estimacion 1"<<endl;
  datosaltos.open("datosaltosdoubles.dat");
  for(int j=1;j<40;j++)
    {
      estimacionalta=estimacionalta+(potencia(basealta,j)/factorial(j));
      diferenciaporcentualalta=100*(valoralto-estimacionalta)/valoralto;
      cout<<"Numero de operacion "<<j<<"; estimacion "<<estimacionalta<<"; diferencia porcentual "<<diferenciaporcentualalta<<endl;
      datosaltos<<diferenciaporcentualalta<<endl;
    }
  datosaltos.close();
}
